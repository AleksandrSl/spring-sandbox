# Spring boot 2 sandbox

Problem with data rest security:

`/users` endpoint mapped to `findAll` method works as it's supposed to,
returning all users for `admin` and current user for `user`.
But `/users/{id}` (e.g. `/users/4`) mapped to `findById` always returns Forbidden
for `user`. I tried different annotations, current is the most insane and 
it still doesn't work. 

The problem is in Kotlin-Java interop for overriding.
For java findById method we can write to methods in kotlin:
```kotlin
override fun findById(id: Long?): Optional<User>


override fun findById(id: Long): Optional<User>
```
The first one with nullable parameters really overrides java version.
But with -Xjsr305=strict compiler argument that is added by spring initialzr the second one is 
the only possible override, though it doesn't work the right way

With current setup (without -Xjsr305=strict and nullable Long) all tests passed.
But if we uncomment compiler arg and change parameter in `findById` in `UserRepository` to not nullable, 
tests will fail, because annotations on this method will no longer work.


Problem with AspectJ compile time weaving for kotlin in IntelliJ

To reproduce:

1. This tests will pass
```
mvn clean test
```

2. `mvn clean` and run `UserItemsSecurityTest.kt` tests from IntelliJ
test `user should see only itself among user of its item` will fail because
security aspect is not weaved into `Item` class. `mvn clean` is important,
because if IntelliJ doesn't recompile `Item` class after the previous step 
aspect is present.
