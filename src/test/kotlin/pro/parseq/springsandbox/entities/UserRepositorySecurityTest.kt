package pro.parseq.springsandbox.entities

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureJdbc
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.ResultSetExtractor
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import kotlin.test.assertEquals

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureJdbc
class UserRepositorySecurityTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var jdbcTemplate: JdbcTemplate

    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `admin should see all users`() {
        assertEquals(4, visibleUsersCount())
    }

    @Test
    @WithMockUser
    fun `user should see only itself`() {
        assertEquals(1, visibleUsersCount())
    }

    @Test
    @WithMockUser
    fun `user should be able to get itself by id`() {
        MockMvcRequestBuilders.get("/users/" + getUserId("user")).run {
            mockMvc.perform(this).andExpect(MockMvcResultMatchers.status().is2xxSuccessful)
        }
    }


    private fun visibleUsersCount(): Int {
        return MockMvcRequestBuilders.get("/users").run {
            mockMvc.perform(this).andExpect(MockMvcResultMatchers.status().is2xxSuccessful)
        }.andReturn().response.contentAsString.let {
            (ObjectMapper().readTree(it)["_embedded"]["users"] as ArrayNode).size()
        }
    }

    private fun getUserId(username: String): Int {

        val id: Int? = jdbcTemplate.query("SELECT user_id FROM users WHERE username = '$username'",
                ResultSetExtractor {
                    it.next()
                    it.getInt("user_id")
                })
        return id ?: throw RuntimeException("User with name $username not found")
    }
}