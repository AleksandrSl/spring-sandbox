package pro.parseq.springsandbox.entities

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import javax.transaction.Transactional
import kotlin.test.assertEquals

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureTestEntityManager
@Transactional
class UserItemsSecurityTest {

    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var entityManager: TestEntityManager

    @Before
    fun setUp() {

        val item = Item().also { entityManager.persist(it) }

        // Add item to user
        User().apply {
            username = "test_user"
            password = "password"
            items.add(item)
        }.also {
            item.users.add(it)
            entityManager.persistAndFlush(it)
        }

        // Add the same item to admin
        userRepository.findByUsername("admin")!!.apply {
            items.add(item)
        }.also {
            item.users.add(it)
            entityManager.persistAndFlush(it)
        }
    }

    @Test
    @WithMockUser(username = "test_user")
    fun `user should see only itself among user of its item`() {
        assertEquals(1, userRepository.findByUsername("test_user")?.let {
            it.items.first().users.size
        })
    }

    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `admin should see all users of any item`() {
        assertEquals(2, userRepository.findAll().first().items.first().users.size)
    }
}