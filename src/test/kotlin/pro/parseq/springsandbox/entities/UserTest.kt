package pro.parseq.springsandbox.entities

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import javax.transaction.Transactional
import kotlin.test.assertEquals

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestEntityManager
@Transactional
class UserTest {

    @Autowired
    lateinit var testEntityManager: TestEntityManager

    @Before
    fun setUp() {
        val userId = User().apply {
            username = "test"
            password = "test"
        }.let {
            testEntityManager.persist(it)
            /*
            testEntityManager.getId(it, Long::class.java)
            throws
            java.lang.IllegalArgumentException: ID mismatch: Object of class [java.lang.Long] must be an instance of long
             */
            testEntityManager.getId(it) as Long
        }
    }

    @Test
    fun test() {
        assertEquals(1, 1)
    }

}

