package pro.parseq.springsandbox.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.session.MapSessionRepository
import org.springframework.session.config.annotation.web.http.EnableSpringHttpSession

@EnableSpringHttpSession
@Configuration
class HttpSession {

    @get: Bean
    val sessionRepository = MapSessionRepository(HashMap()).apply {
        setDefaultMaxInactiveInterval(100)
    }
}