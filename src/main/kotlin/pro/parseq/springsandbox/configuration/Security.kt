package pro.parseq.springsandbox.configuration

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.AdviceMode
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.sql.DataSource

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
class GeneralSecurity(
        @Autowired @Qualifier("dataSource") private val appDataSource: DataSource
) : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        http
                .anonymous().disable()
                .authorizeRequests().anyRequest().authenticated()
                .and()
                .httpBasic().authenticationEntryPoint(authenticationEntryPointProvider)
                .and()
                .logout().permitAll()
                .clearAuthentication(true).invalidateHttpSession(true)
                .logoutRequestMatcher(AntPathRequestMatcher("/logout", HttpMethod.POST.toString()))
                .logoutSuccessHandler { _, response, _ ->
                    response.apply {
                        status = HttpServletResponse.SC_OK
                        writer.println("You have been successfully logged out from the application")
                    }
                }
                .and()
                .csrf().disable().sessionManagement()
                .invalidSessionStrategy { _: HttpServletRequest, response: HttpServletResponse ->
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Your session has been invalidated")
                }
    }

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.authenticationProvider(authenticationProvider.apply {
            setPasswordEncoder(BCryptPasswordEncoder())
        })
    }

    /*
     * Initialization of beans in constructor raises `BeanFactory has not been injected into @Configuration class`
     * error. This property forces initialization of `userDetailsServiceProvider` bean,
     * therefore this property must be initialized outside the constructor.
     */
    @get: Bean
    val authenticationProvider by lazy {
        DaoAuthenticationProvider().apply {
            setUserDetailsService(userDetailsServiceProvider)
        }
    }

    @get: Bean
    val userDetailsServiceProvider = JdbcDaoImpl().apply {
        dataSource = appDataSource
    }

    @get: Bean
    val authenticationEntryPointProvider = AuthenticationEntryPoint { _, response, _ ->
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "You must be authenticated to access the application")
    }

    @get: Bean
    val securityEvaluationContextExtensionProvider = SecurityEvaluationContextExtension()
}

/*
 Neat trick. For some reasons AspectJSecurityInterceptor can not be used before aopalliance.MethodSecurityInterceptor.
 And it turned out that beans are instantiated in alphabet order. Therefore this configuration bean should have
 name that is lexicographically greater than bean above.
 */
@EnableGlobalMethodSecurity(prePostEnabled = true, mode = AdviceMode.ASPECTJ)
@Configuration
class MethodAspectJSecurity
