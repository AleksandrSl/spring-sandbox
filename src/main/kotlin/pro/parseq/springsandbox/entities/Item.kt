package pro.parseq.springsandbox.entities

import org.springframework.security.access.prepost.PostFilter
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToMany
import javax.persistence.Table

@Entity
@Table(name = "items")
class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "item", nullable = false)
    val id: Long? = null

    @Column
    lateinit var name: String

    /*
    NOTE: It's important to know that for slave object get method is invoked,
    but for the master entity collection get is not invoked so it isn't possible to secure collections
    from master entity.
     */
    @Column
    @ManyToMany(mappedBy = "items")
    val users: MutableSet<User> = mutableSetOf()
        @PostFilter("filterObject.username == principal.username or hasRole('ADMIN')")
        get
}