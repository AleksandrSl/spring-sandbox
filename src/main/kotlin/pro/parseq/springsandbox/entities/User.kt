package pro.parseq.springsandbox.entities

import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.Table

@Entity
@Table(name = "users")
class User{

    @Column(unique = true, nullable = false)
    lateinit var username: String

    @Column(nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    lateinit var password: String

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    var id: Long? = null

    @Column(nullable = false)
    var enabled: Boolean = true

    @ManyToMany
    @JoinTable(
            name = "user_items",
            joinColumns = [(JoinColumn(name = "user_id", nullable = false))],
            inverseJoinColumns = [(JoinColumn(name = "item_id", table = "items", nullable = false))]
    )
    val items: MutableSet<Item> = mutableSetOf()
}