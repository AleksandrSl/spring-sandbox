package pro.parseq.springsandbox.entities

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.security.access.prepost.PostAuthorize
import org.springframework.security.access.prepost.PreAuthorize
import java.util.Optional


@PreAuthorize("hasRole('ADMIN')")
interface UserRepository : CrudRepository<User, Long> {

    @PreAuthorize("permitAll()")
    @Query("select u from User u where u.username like ?#{ hasRole('ADMIN') ? '%' : principal.username }")
    override fun findAll(): MutableIterable<User>

    /*
    Both functions below are valid form the compiler point of view.
    But first one doesn't really override anything
    And if <arg>-Xjsr305=strict</arg> is added to kotlin compiler
    the second one becomes invalid override, though it is the right one really
     */
//    @PreAuthorize("permitAll()")
//    @PostAuthorize("returnObject?.username == principal.username or hasAuthority('ADMIN')")
//    override fun findById(id: Long): Optional<User>

    @PreAuthorize("permitAll()")
    @PostAuthorize("returnObject.get().username == principal.username or hasRole('ADMIN')")
    override fun findById(id: Long?): Optional<User>

    @PreAuthorize("permitAll()")
    fun findByUsername(@Param("username") username: String): User?
}