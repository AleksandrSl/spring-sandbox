BEGIN;

INSERT INTO users (username, password, enabled) VALUES
    ('admin', '$2a$04$S8CVefipVwPQqBES0BIJtO/Eh0TuT3rHJ42SA6ovXdn8VW2deDb4K', TRUE),
    ('supervisor', '$2a$04$S8CVefipVwPQqBES0BIJtO/Eh0TuT3rHJ42SA6ovXdn8VW2deDb4K', TRUE),
    ('tester', '$2a$04$S8CVefipVwPQqBES0BIJtO/Eh0TuT3rHJ42SA6ovXdn8VW2deDb4K', TRUE),
    ('user', '$2a$04$S8CVefipVwPQqBES0BIJtO/Eh0TuT3rHJ42SA6ovXdn8VW2deDb4K', TRUE);


CREATE TABLE IF NOT EXISTS authorities (
    username  varchar(50) NOT NULL,
    authority varchar(50) NOT NULL,
    CONSTRAINT fk_authorities_users FOREIGN KEY (username) REFERENCES users (username)
);

INSERT INTO authorities (username, authority)
VALUES ('admin', 'ROLE_ADMIN'), ('user', 'ROLE_USER');

COMMIT;